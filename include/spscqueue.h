#ifndef SPSCQUEUE_H
#define SPSCQUEUE_H

#include <iostream>
#include <numeric>
#include <memory>
#include <queue>
#include <mutex>
#include <condition_variable>
#include <thread>


namespace mySPSC
{

using uint = uint64_t;

constexpr uint operator""_MB(uint t)
{
	return t << 20;
}

enum class Status
{
	WORKING,
	FULL,
	COMPLETE
};

struct Message
{
	std::size_t size;
	const uint* pointer;
};

class SPSCQueue
{

public:
	SPSCQueue();

	Status runThreads(std::size_t targetMemVolume = 256_MB, std::size_t freq = 1);


private:
	void produce(std::size_t freq = 1);
	void consume();

	class Buffer;

	std::unique_ptr<Buffer> buffer;
	std::queue<Message> queue;
	std::mutex mtx;
	std::condition_variable cv;
	std::size_t inputDataMaxVolume = 0;
	std::size_t megabytesConsumed = 0;
	Status status = Status::WORKING;

	const std::size_t MAX_QUEUE_SIZE = 256;
	const char* filename = "filename.b";
};

class SPSCQueue::Buffer
{
public:
	Buffer() : buffer(nullptr) {};

	void allocate()
	{
		if (!buffer)
			buffer = new uint[size];
	}

	void put(uint value)
	{
		if (!buffer)
			allocate();
		buffer[index++ & MASK] = value;
	}

	const uint *data() const
	{
		return buffer ? (buffer + (index & MASK)) : nullptr;
	}

	std::size_t currentIndex() const
	{
		return index;
	}

	void print() const
	{
		for (std::size_t i = size - 2; i < size; i++)
			std::cout << buffer[i] << std::endl;
	}

	~Buffer()
	{
		if (buffer)
			delete[] buffer;
	};

private:
	uint *buffer;
	std::size_t index = 0;

	const std::size_t size = 256_MB / sizeof(*buffer);
	const std::size_t MASK = size - 1;
};

}


#endif // SPSCQUEUE_H
