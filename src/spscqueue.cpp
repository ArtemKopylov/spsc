#include <chrono>
#include <fstream>
#include "../include/spscqueue.h"

using namespace mySPSC;
using namespace std;
using namespace chrono;


SPSCQueue::SPSCQueue() :
	buffer(make_unique<Buffer>())
{

}

Status SPSCQueue::runThreads(std::size_t targetMemVolume, std::size_t freq)
{
	inputDataMaxVolume = targetMemVolume;

	auto producer = std::thread(&SPSCQueue::produce, this, freq);
	auto consumer = std::thread(&SPSCQueue::consume, this);

	producer.join();
	consumer.join();

	return status;
}

void SPSCQueue::produce(std::size_t freq)
{
	buffer->allocate();
	auto start = high_resolution_clock::now();
	auto stop = high_resolution_clock::now();
	auto period = nanoseconds(duration_cast<nanoseconds>(1s).count() / freq);

	while (true)
	{
		start = high_resolution_clock::now();
		Message m = {1_MB / sizeof(uint), buffer->data()};

		unique_lock<mutex> ulock(mtx);

		if (status != Status::WORKING || queue.size() == MAX_QUEUE_SIZE)
		{
			if (queue.size() == MAX_QUEUE_SIZE)
				status = Status::FULL;

			break;
		}
		uint offset = buffer->currentIndex();
		for (std::size_t i = 0; i < m.size; i++)
		{
			buffer->put(i + offset);
		}
		queue.emplace(m);

		ulock.unlock();
		cv.notify_one();

		stop = high_resolution_clock::now();

		this_thread::sleep_for(period - (stop - start));
	}
}

void SPSCQueue::consume()
{
	ofstream fstream(filename, ios::binary);
	auto start = high_resolution_clock::now();
	auto period = duration_cast<seconds>(high_resolution_clock::now() - start);

	while (true)
	{
		unique_lock<mutex> ulock(mtx);
		cv.wait(ulock, [this]{return !queue.empty();});
		if (!queue.empty())  // while (!queue.empty())
		{
			Message m = queue.front();
			queue.pop();

			fstream.write(reinterpret_cast<const char*>(m.pointer), m.size * sizeof(uint));
//			if (!fstream)
//			{
//				status = Status::FERROR;
//			}

			megabytesConsumed += (m.size * sizeof(uint)) >> 20;
			period = duration_cast<seconds>(high_resolution_clock::now() - start);
			if (period.count() != 0)
			{
				cout << "\rQueue size: " << queue.size() << " messages. Writing speed ~ " << (double)(megabytesConsumed) / period.count() << " MB/s" << "       " << flush;
			}

			if (megabytesConsumed >= inputDataMaxVolume)
			{
				status = Status::COMPLETE;
				break;
			}
			else if (status != Status::WORKING)
			{
				break;
			}
		}
		ulock.unlock();
	}

	fstream.close();
}

