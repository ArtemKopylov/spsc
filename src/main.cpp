#include <iostream>
#include <fstream>
#include "../include/spscqueue.h"

using namespace mySPSC;
using namespace std;

int main()
{
	SPSCQueue q;
	switch	(q.runThreads(4096u, 10))
	{
	case Status::COMPLETE:
		cout << endl << "Target input data volume reached, program complete" << endl;
		break;
	case Status::FULL:
		cout << endl << "Queue overflow, program terminated" << endl;
		break;
	default:
		cout << endl << "Ooops! Something went wrong" << endl;
		break;
	}
}
